<?php
/**
 * Created by PhpStorm.
 * User: yihui
 * Date: 15/7/23
 * Time: 下午1:25
 */
defined('SYSPATH') or die('No direct script access.');
define('APPLICATION_PREPATH', DOCROOT . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);

function dd()
{ //[dm]
    if (func_num_args() === 0)
        return;

    // Get all passed variables
    $variables = func_get_args();
    if(!empty($_SERVER['HTTP_USER_AGENT'])){
        $ret = call_user_func_array(array('Debug', 'vars'), $variables);
    }else{
        $ret=debug_zval_dump($variables);
    }
    echo $ret;
    die;
}

function d()
{ //[m] [fm]
    if (func_num_args() === 0)
        return;

    // Get all passed variables
    $variables = func_get_args();
    if(!empty($_SERVER['HTTP_USER_AGENT'])){
        $ret = call_user_func_array(array('Debug', 'vars'), $variables);
    }else{
        $ret=debug_zval_dump($variables);
    }
    echo $ret;
}

//载入全局函数
require_once 'function.php';

function url(array $params, $route_name = null)
{ //[url]
    if (is_null($route_name)) {
        if (empty($params['controller'])) {
            $params['controller'] = Request::current()->controller();
        }
        if (empty($params['action'])) {
            $params['action'] = Request::current()->action();
        }

        $route_name = $params['controller'] . '_' . $params['action'];
    } else {
        // var_dump($route_name);
        // 	die;
    }


// $_routes_all = Route::all();
// foreach($_routes_all as $route) {
// 	var_dump( $route->_uri);
// 	echo "<br/>";
// }
// die;

    if (@$params['controller'] == 'ask' AND @$params['action'] == 'detail') {
        require_once APPPATH . 'classes/model/url.php';

        return Model_Url::ask_detail($params, $route_name);
    }

    if (@$params['controller'] == 'topic' AND @$params['action'] == 'detail') {
        require_once APPPATH . 'classes/model/url.php';

        return Model_Url::topic_detail($params, $route_name);
    }

    if ('admin' == $route_name
        OR in_array(@$params['controller'], array('admin', 'search', 'home'), true)
    ) {
        require_once APPPATH . 'classes/model/url.php';

        return Model_Url::uri($params, $route_name);
    } else {
        $_routes_all = Route::all();
        if (isset($_routes_all[$route_name])) {
            $route = Route::get($route_name);
        } else {
            if (empty($params['controller'])) {
                $params['controller'] = Request::current()->controller();
            }

            if (isset($_routes_all[$params['controller']])) {
                $route = Route::get($params['controller']);
            } else {
                $route = Route::get('default_route');
            }
        }

        return '/' . $route->uri($params);
    }
}//END func url

