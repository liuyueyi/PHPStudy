<?php
/**
 * Created by PhpStorm.
 * User: yihui
 * Date: 15/7/19
 * Time: 下午9:50
 */
//echo "current dir is : ", getcwd() , "<br/>";
//echo "change current dir to /Users/yihui/==>", chdir("/Users/yihui/"), " ===changed===>", getcwd(), "<br/>";

function get_all_under_dir(){
    $dir = "/Users/yihui/Project/";
    $handle = opendir($dir);
    if($handle != false){
        echo "under this dir: <br/>";
        while(false != ($file = readdir($handle))){
            echo $file."<br/>";
        }
        closedir($handle);
    }else{
        echo "failt to open this dir<br/>";
    }

    //  another function
    $dirs = scandir($dir);
    print_r("=====use scandir funciton====<br\>");
    print_r($dirs);
}
//get_all_under_dir();

function file_test(){
    // open file
    $path = "file_test.txt";
    $f = fopen($path, "w+");
    fwrite($f, "write some text into the file by fwrite function");
    fclose($f);
    // then read
    $f = fopen($path, "r");
    $content = fread($f, filesize($path)); // get file content, the latter arg is read length
    print $content;
    print "\n";
    fclose($f);

    //新建零食文件
    $temp_file = tmpfile();
    fwrite($temp_file, "write some var or data into this temp file");
    fseek($temp_file, 0); //将文件指针只想文件头
    echo fread($temp_file, 1024), "<br/>";
    fclose($temp_file);

    // 或者利用 tempnam()新建具有唯一文件名的文件，用法与上面类似，只是需要删除 unlink($file);
    // tempnam 在指定的目录内建立一个唯一名的文件，若指定目录不存在，则在系统临时文件夹下生成一个，并返回文件名
    // tempnam("/Users/yihiu/", "tempfname");
    // fopen($tempfname, "r") 打开生成的临时文件

    // delete file ==> unlink($file_handle);
    // copy file ===> copy($sourFileName, $targetFileName), and return boolean
    // move rename ==> rename($originFileName, $newFilename);
}
file_test();

function file_input(){
    $path = "file_test.csv";
    $ary = array(array("name", "passwd", "bumen", "zhiwei"),
        array("user1", "123", "mishu", "keyuan"),
        array("user2", "2312", "mushu", "keyuan"),
        array("user3", "avsadf", "shuji", "keyuan")
        );
    $fp = fopen($path, "w");
    foreach ($ary as $line) {
        fputcsv($fp, $line); // 将指定的数组格式转化为csv文件
    }
    fclose($fp);

    // 直接像文件中写入字符串
    file_put_contents($path, "user4, woca, it, study");

    // reade file
    echo "====read file====<br/>";
    $fp = file($path);
    foreach($fp as $line){
        echo $line."<br/>";
    }

    // 配置文件解析 parse_ini_file()
    // fgetss 读取一行数据，并过滤掉html和pho标记
    // 处理上传文件： move_uploaded_file($_FILE["file"]["temp_name"], $dir.$filename)
    // 下载文件 fpassthru()
}