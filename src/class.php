<?php
/**
 * Created by PhpStorm.
 * User: yihui
 * Date: 15/7/19
 * Time: 上午11:11
 */
class Test{
    public $TT = "hello Test";
    public $ary = array(10, 20, 30, 40);

    public function print_test(){
        //$this-> 访问内部成员
        var_dump($this->ary);
        var_dump($this->TT);
    }
}

class MyTest extends Test{
    public static $s_value = "static value";
    const TAG = "CONST:: my test class";
    public $TT = "hello myTest";
    public $type = "my test";

    public static function s_func_test(){
        echo MyTest::$s_value, "<br/>";
    }

    public function print_test(){
        parent::print_test();
        var_dump($this->type);
        // var_dump(self::TAG); 和下面的等价
        var_dump(MyTest::TAG); // 常量用类名来访问
        echo "======print over======", "<br/>";
    }

    public function print_s_value(){
        // 访问静态属性
        var_dump(MyTest::$s_value);
    }


    /*
     * clone 对象时被调用
     */
    public function __clone(){
        $this->type = "new cloned object";
    }

    /*
     * 序列化执行之前调用，返回待序列化元素组成的数组
     * 静态属性和常量应该被踢出数组
     */
    public function __sleep(){
        return array("TT", "type");
    }

    /*
     * 反序列化之前调用，用于恢复之前丢失的资源
     */
    public function __wakeup(){
        $this->type = "recover object";
    }
}

$test = new MyTest();
$test->print_test();// ouput hello mytest
MyTest::s_func_test(); // static function call;

$newTest = clone $test; // clone对象,且不会修改愿对象
$newTest->print_test();

// php 对象序列化和反序列化
$demo = serialize($newTest);
echo "after serialize the NewTest object: ", $demo, "<br/>";
$recv = unserialize($demo);
echo $recv->print_test();