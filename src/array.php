<?php
/**
 * Created by PhpStorm.
 * User: yihui
 * Date: 15/7/19
 * Time: 上午10:06
 */

/*
 * 数组增删改
 */
function array_basic(){
    // 初始化数组，其中不显示加入key的元素会默认添加一个下标，如下面的 6对应的是women
    $ary = array(5=>"nihao", "women", "jet"=>"dou", "shi", "hao", "hai", "zi");
    var_dump($ary);
    // 遍历输出结果
    foreach($ary as $key=>$value){
        echo $key.":".$value, "<br/>";
    }
    // 最后新增一个元素
    $ary[] = "add value";
    // 删除数组中的一个元素
    unset($ary[6]);
    // 修改一个元素的值
    $ary[7] = "new value";
    var_dump($ary);

    // 两个数组形成一个数组, 前面一个做为key。后面一个作为value
    $new_arty = array_combine(array(2, 3, 4, 5, 7), array("one", "tow",
        "three", "four", "five"));
    echo "----compiled array-----", "<br/>";
    var_dump($new_arty);
}
array_basic();

function array_key_value(){
    echo "=====array key value test======<br/>";
    $ary = array(12=>"nihao", "women"=>13);
    $key = 12;
    $value = 13;
    echo in_array($value, $ary), "<br/>";
    echo array_key_exists($key, $ary), "<br/>";
    echo key($ary), " ===> current keys<br/>";
    echo "next() 下移数组内部元素指针<br/>";
    next($ary);
    echo key($ary), "====>next key<br/>";

    print_r(array_keys($ary)); // 所有key
    print_r(array_values($ary)); // all values
    array_flip($ary); // key value反转
    array_splice($ary, 2, 0, 7); // 值替换
    array_unique($ary); // 去除重复的值

    $test = array(
        'key' => 124,
        'k2' =>256
    );
    var_dump($test);
    unset($test['k2']); // 删除array中的一对元素
    var_dump($test);
}

array_key_value();

function array_iter_test(){
    echo "=====array iterator======<br/>";
    // 数组遍历
    $ary = array(5=>"nihao", "women", "jet"=>"dou", "shi", "hao", "hai", "zi");
    // list()将当前数组单元转换为变量使用
    // each()返回数组中的键值
    while(list($key, $value) = each($ary)){
        list($ans) = $value;
        list($index) = $key;
        echo "$index : $ans ";
    }
    echo "<br/>";

    foreach($ary as $key => $value){
        echo "$key : $value";
    }
    echo "<br/>";
}
array_iter_test();

class TestObj{
    var $hello = "nihao";
    var $world = "shijie";
    public function setHello($hello){
        $this->hello = $hello;
    }
    public function getHello(){
        return $this->hello;
    }

    public function setWolrd($world){
        $this->world = $world;
    }
    public function getWolrd(){
        return $this->world;
    }
}

/*
 * type transfer
 */
function array_transfer(){
    $i = 12;
    $ch = "hello";
    // 对象转为数组时，属性作为数组元素，属性名为键，value为值
    $obj = new TestObj();
    $obj->setHello("你好");

    var_dump((array)$i);
    var_dump((array)$ch);
    var_dump((array)$obj);
    var_dump((array)true);
    var_dump((array)false);
}

array_transfer();