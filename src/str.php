<?php
/**
 * Created by PhpStorm.
 * User: yihui
 * Date: 15/7/21
 * Time: 下午12:00
 */

function str_test()
{
    $str1 = "S";
    $str2 = "  s ";

    // 忽略大小写的比较字符串, 相等则返回0
    print strcasecmp(trim($str1), trim($str2)) . "<br/>";

    // 正则匹配
    $str3 = array("175b", "170", "165c", "175B", "160d", "c");
    $pattern = '/\d*[cCdDeEfFgG]/';
    var_dump($str3);
    foreach ($str3 as $str) {
        if (preg_match($pattern, $str, $matches) && !empty($matches)) {
            echo $str, "===>", var_dump($matches), "<br/>";
        }
    }
}

// 从图片的url中获取图片的size
function get_img_width_height($srcfile)
{
    preg_match('/\w_(?P<w>\d+)x(?P<h>\d+)\./', $srcfile, $m);
    return $m;
}

echo "=====get size from img url=====";
$srcfile = "/p1/150601/upload_ie2deytghe3genjygezdambqgiyde_750x140.jpg";
$result = get_img_width_height($srcfile);
var_dump($result);


// 判断一个字符串是否为另一个字符串的子串
function str_pos_test()
{
    $imgs = ['floatImage1', 'floatImage2', 'title', 'floatImage3'];
    foreach ($imgs as $key => $value) {
        if (strpos($value, "floatImage") !== false) { // 表示是子串
            echo $value, "<br/>";
        }
    }
}

// 数字格式化输出，果然还是qq输入法给力多了，默认的输入发输入速度真是坑爹
$number = 123450;
echo "$" . number_format($number, 2);

function str_replace_test()
{
    $base = "<img href=%url% />";
    // 将$base中的%url%替换成第二个参数
    $ans = str_replace("%url%", "http://www.baidu.com", $base);
    var_dump($ans);

    //将第三个参数中的满足数组中的元素替换为第二个参数的内容
    $base = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U");
    $ans = str_replace($base, "", "Hello from I php");
    var_dump($ans);

    // Provides: You should eat pizza, beer, and ice cream every day
    $phrase = "You should eat fruits, vegetables, and fiber every day.";
    $healthy = array("fruits", "vegetables", "fiber");
    $yummy = array("pizza", "beer", "ice cream");

    $newphrase = str_replace($healthy, $yummy, $phrase);
    var_dump($newphrase);


    // test
    $esi = [
        '##ESI##' => "<esi>hello world</esi>"
    ];
    $base = 'status: nihao, Data: ##ESI##: women';
    $ans = str_replace(array_keys($esi), $esi, $base);
    var_dump($ans);

    // another test
    $test = 'hello\'wxj\' "wzb"';
    $ans = addslashes($test);
    var_dump($ans);
    $ans = str_replace('\'', '\\', $ans);
    var_dump($ans);
}

echo "====> str_replace_test =======<br/>";
str_replace_test();

// 页面跳转
header("Location: http://www.baidu.com");
